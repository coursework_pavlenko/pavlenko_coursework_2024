﻿using System;

namespace ClassLibrary1
{
    public class Buses
    {
        public int Id { get; set; }
        public string BusNumber { get; set; }
        public string Source { get; set; }
        public string Destination { get; set; }
        public string Type { get; set; }
        public string ArrivalTime { get; set; }
        public decimal Price { get; set; }

        public Reservation Reservation
        {
            get => default;
            set
            {
            }
        }

        public Buses(int id, string busNumber, string source, string destination, string type, string arrivalTime, decimal price)
        {
            Id = id;
            BusNumber = busNumber;
            Source = source;
            Destination = destination;
            Type = type;
            ArrivalTime = arrivalTime;
            Price = price;
        }
    }
}
