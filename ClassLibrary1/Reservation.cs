﻿using System;
using System.Data;
using System.Data.OleDb;

namespace ClassLibrary1
{
    public class Reservation
    {
        private OleDbConnection con;

        public Reservation(string connectionString)
        {
            con = new OleDbConnection(connectionString);
        }

        public Ticket Ticket
        {
            get => default;
            set
            {
            }
        }

        public DataTable GetReservations()
        {
            con.Open();
            OleDbCommand cmd = new OleDbCommand("select * from passenger", con);
            OleDbDataAdapter da = new OleDbDataAdapter();
            da.SelectCommand = cmd;
            DataTable dt = new DataTable();
            da.Fill(dt);
            con.Close();
            return dt;
        }
    }
}
